import {
  CanActivate,
  createParamDecorator,
  ExecutionContext,
  Injectable,
  NestMiddleware,
  SetMetadata,
} from "@nestjs/common";
import { UnauthorizedException } from "@nestjs/common/exceptions";
import { Reflector } from "@nestjs/core";
import { Observable } from "rxjs";

const AUTH_TYPE_ANONYMOUS = "anonymous";
const AUTH_TYPE_AUTHENTICATED = "authenticated";

export const Anonymous = (): ReturnType<typeof SetMetadata> =>
  SetMetadata("auth:type", AUTH_TYPE_ANONYMOUS);

export const Authenticated = (): ReturnType<typeof SetMetadata> =>
  SetMetadata("auth:type", AUTH_TYPE_AUTHENTICATED);

export const Permissions = (
  ...permissions: string[]
): ReturnType<typeof SetMetadata> => SetMetadata("auth:scope", permissions);

export class AuthMiddleware implements NestMiddleware {
  constructor() {}
  use(req: any, res: any, next: () => void) {
    if (req && req.cookies && req.cookies.token) {
      console.log("cumple middleware");
      if (next) {
        next();
      }
      /* this.userService.getTokenInfo(req.cookies.token).then(u => {
          req.user = u;
          if (next) {
            next();
          }
        }); */
    } else if (next) {
      next();
    }
  }
}

@Injectable()
export class AuthGuard implements CanActivate {
  constructor(private readonly reflector: Reflector) {}

  canActivate(
    context: ExecutionContext,
  ): boolean | Promise<boolean> | Observable<boolean> {
    // decorators on methods are prior to those on class
    const authType =
      this.reflector.get<string>("auth:type", context.getHandler()) ||
      this.reflector.get<string>("auth:type", context.getClass());
    if (!authType) {
      return true;
    }
    const req = context.switchToHttp().getRequest();

    const { user } = req;
    if (!user && authType === AUTH_TYPE_AUTHENTICATED) {
      throw new UnauthorizedException();
    }
    return true;
  }
}

export const ReqUser = createParamDecorator((data, req) => {
  return req.user;
});

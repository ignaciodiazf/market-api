import { Inject, Injectable, LoggerService, Module } from "@nestjs/common";
import pino from "pino";
import { Logger, QueryRunner } from "typeorm";
import env from "./config";

export const createPino = (): ReturnType<typeof pino> => {
  let levelValue: string;
  if (env.isDev) {
    levelValue = "trace";
  } else {
    levelValue = env.isTest ? "silent" : "info";
  }
  return pino({
    prettyPrint: env.isDev,
    level: levelValue,
  });
};

@Injectable()
export class PinoLoggerService implements LoggerService {
  public logger: pino.Logger;
  constructor() {
    this.logger = createPino();
  }
  public trace(message: string, ...args: any[]): void {
    this.logger.trace(message, ...args);
  }

  public debug(message: string, ...args: any[]): void {
    this.logger.debug(message, ...args);
  }

  public info(message: string, ...args: any[]): void {
    this.logger.info(message, ...args);
  }

  public warn(message: string, ...args: any[]): void {
    this.logger.warn(message, ...args);
  }

  public error(message: string, ...args: any[]): void {
    this.logger.error(message, ...args);
  }

  public log(message: string, ...args: any[]): void {
    this.logger.info(message, ...args);
  }
}

@Injectable()
export class TypeOrmLoggerService implements Logger {
  constructor(
    @Inject(PinoLoggerService) private readonly logger: pino.Logger,
  ) {}
  logQuery(query: string, parameters?: any[], queryRunner?: QueryRunner) {
    this.logger.trace(`typeorm:query ${query}`, parameters || []);
  }
  logQueryError(
    error: string | Error,
    query: string,
    parameters?: any[],
    queryRunner?: QueryRunner,
  ) {
    this.logger.error(`typeorm:query ${error}`, {
      query,
      parameters: parameters || [],
    });
  }
  logQuerySlow(
    time: number,
    query: string,
    parameters?: any[],
    queryRunner?: QueryRunner,
  ) {
    this.logger.warn(`typeorm:query slow +${time}`, {
      query,
      parameters,
    });
  }
  logSchemaBuild(message: string, queryRunner?: QueryRunner) {
    this.logger.info(`typeorm:schema ${message}`);
  }
  logMigration(message: string, queryRunner?: QueryRunner) {
    this.logger.info(`typeorm:migration ${message}`);
  }
  log(level: "log" | "info" | "warn", message: any, queryRunner?: QueryRunner) {
    switch (level) {
      case "log":
        this.logger.debug(message);
        break;
      case "info":
        this.logger.info(message);
        break;
      case "warn":
        this.logger.warn(message);
        break;
    }
  }
}

@Module({
  imports: [],
  providers: [PinoLoggerService, TypeOrmLoggerService],
  exports: [PinoLoggerService, TypeOrmLoggerService],
})
export class LoggerModule {}

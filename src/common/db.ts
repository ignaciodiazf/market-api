import { Injectable, Module } from "@nestjs/common";
import { InjectConnection, TypeOrmModule } from "@nestjs/typeorm";
import { Connection } from "typeorm";
import env from "./config";
import { LoggerModule, TypeOrmLoggerService } from "./logger";
import { requireClassesSync } from "./loader";

@Injectable()
export class DbService {
  constructor(@InjectConnection() public readonly connection: Connection) {}
}

@Module({
  imports: [
    TypeOrmModule.forRootAsync({
      imports: [LoggerModule],
      inject: [TypeOrmLoggerService],
      useFactory: (logger: TypeOrmLoggerService) => ({
        type: "postgres",
        host: env.DB,
        database: env.DB_DATABASE,
        username: env.DB_USER,
        password: env.DB_PASSWORD,
        port: env.DB_PORT,
        migrationsRun: !env.isProd,
        synchronize: env.isDev,
        logging: true,
        logger,
        entities: requireClassesSync(__dirname, "../entity"),
        migrations: requireClassesSync(__dirname, "../migration"),
        subscribers: requireClassesSync(__dirname, "../subscriber"),
      }),
    }),
    TypeOrmModule.forFeature(requireClassesSync(__dirname, "../entity")),
  ],
  providers: [DbService],
  exports: [DbService, TypeOrmModule],
})
export class DbModule {}

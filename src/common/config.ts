import * as envalid from "envalid";
require("dotenv").config();
const env = envalid.cleanEnv(process.env, {
  DB: envalid.str({
    default: "127.0.0.1",
    devDefault: "127.0.0.1",
  }),
  DB_PORT: envalid.num({ default: 5432 }),
  DB_USER: envalid.str({ default: "postgres", devDefault: "postgres" }),
  DB_PASSWORD: envalid.str({ default: "admin", devDefault: "admin" }),
  DB_DATABASE: envalid.str({ default: "market", devDefault: "market" }),
  PORT: envalid.port({ default: 80, devDefault: 3000 }),
  JWT_TOKEN: envalid.str({ default: "secret", devDefault: "secret2" }),
  NODE_ENV: envalid.str({
    choices: ["development", "test", "production", "staging"],
    default: "production",
    devDefault: "development",
  }),
});

export default env;

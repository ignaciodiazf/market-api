import { Inject, Injectable } from "@nestjs/common";
import env from "./config";
import { sign, verify } from "jsonwebtoken";
import { InvalidTokenException } from "./errors";

interface TokenData {
  username: string;
}

@Injectable()
export class TokenService {
  private jwtToken: string;

  constructor() {
    this.jwtToken = env.JWT_TOKEN;
  }

  public sign(): string {
    const data: TokenData = {
      username: "admin",
    };
    return sign(data, this.jwtToken, {
      expiresIn: "7d",
    });
  }

  public verify(token: string): string {
    try {
      return (verify(token, this.jwtToken) as TokenData).username;
    } catch {
      throw new InvalidTokenException();
    }
  }
}

import { MiddlewareConsumer, Module, RequestMethod } from "@nestjs/common";
import { APP_GUARD } from "@nestjs/core";
import { AuthGuard, AuthMiddleware } from "./common/authentication";
import { DbModule, DbService } from "./common/db";
import { LoggerModule } from "./common/logger";
import { TokenService } from "./common/token";
import { AppController } from "./controller/app.controller";
import { AppService } from "./service/app.service";

@Module({
  imports: [LoggerModule, DbModule],
  controllers: [AppController],
  providers: [
    {
      provide: APP_GUARD,
      useClass: AuthGuard,
    },
    DbService,
    TokenService,
    AppService,
  ],
})
export class AppModule {
  public configure(consumer: MiddlewareConsumer): void {
    consumer
      .apply(AuthMiddleware)
      .forRoutes({ path: "*", method: RequestMethod.ALL });
  }
}

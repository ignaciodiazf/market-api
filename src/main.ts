import { NestFactory } from "@nestjs/core";
import { AppModule } from "./app.module";
import env from "./common/config";
import { AppExceptionFilter, GeneralExceptionFilter } from "./common/errors";
import { PinoLoggerService } from "./common/logger";

async function bootstrap(): Promise<void> {
  const app = await NestFactory.create(AppModule);
  app.useLogger(app.get(PinoLoggerService));
  app.useGlobalFilters(new GeneralExceptionFilter(), new AppExceptionFilter());
  await app.listen(env.PORT);
}
bootstrap().then(() => {
  console.log(env.JWT_TOKEN);
});

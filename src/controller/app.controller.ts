import { Controller, Get } from "@nestjs/common";
import { AppService } from "../service/app.service";
import env from "../common/config";
@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get()
  getHello(): string {
    console.log(env.isProduction);
    return this.appService.getHello();
  }
}
